Rails.application.routes.draw do
  devise_for :users

  resources :beers do
  	resources :likes, except: [:index], controller: 'beers/likes'
  	# delete '/likes/:id', to: 'beers/likes#removelike'
  end

  get 'about', to: 'pages#about'

  root 'pages#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
