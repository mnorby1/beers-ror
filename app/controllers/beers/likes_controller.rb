class Beers::LikesController < ApplicationController

	def create

		unless current_user
			# TODO: Alert the user they must be signed in to like a beer
			return redirect_to beers_path
		end

		@beer = Beer.find(params[:beer_id])

		likeCount = Like.where(beer_id: @beer.id, user_id: current_user.id).count 

		unless likeCount == 0
			# You can only like a beer once
			# TODO: Alert the user they can only like once
			return redirect_to beers_path
		end

		@like = Like.new
		@like.beer = @beer
		@like.user = current_user
		@like.like = true

		@like.save

		redirect_to beers_path
	end

	def destroy

		like = Like.find(params[:id])

		like.destroy

		redirect_to beers_path
	end
end