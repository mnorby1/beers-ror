class BeersController < ApplicationController

	def index
		@beer = Beer.new
		@beers = Beer.all

	end

	def create

		@beer = Beer.new(beer_params)

		if current_user
			puts "The current_user exists: #{current_user.as_json}"
			@beer.user = current_user
		end
		if @beer.save
			puts "The beer saved successfully: #{@beer.as_json}"
			redirect_to beers_path
		else
			puts "The beer failed: #{@beer.errors.as_json}"
		end
	end

	def edit

		@beer = Beer.find(params[:id])
	end

	def update

		@beer = Beer.find(params[:id])

		if @beer.update(beer_params)
			puts "The beer has been updated: #{@beer.as_json}"
			redirect_to beers_path
		else
			puts "The beer update failed: #{@beer.errors.as_json}"
		end
	end

	def destroy
		@beer = Beer.find(params[:id])

		if @beer.destroy
			puts "The beer has been deleted: #{@beer.as_json}"
			redirect_to beers_path
		else
			puts "The beer delete failed: #{@beer.errors.as_json}"
		end
	end

	private

	def beer_params
		params.require(:beer).permit(:name, :description)
	end
end
